﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Net;
using System.Net.Http;
using System.Xml.Linq;
using static System.Net.WebRequestMethods;

namespace Boundaries
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the location");
            string userLocationInput = Console.ReadLine();
            GetLocation(userLocationInput);
        }

        public static void GetLocation(string userLocationInput)
        {
            WebClient client = new WebClient();
            try
            {
                string content = (string)client.DownloadString(
                "https://api.opencagedata.com/geocode/v1/json?q=" + userLocationInput + "&key=22f7174ccf6a4961979059813cc8cad3");
                dynamic jObject = JsonConvert.DeserializeObject(content);
                Console.WriteLine("Latitude : " + jObject["results"][0]["geometry"]["lat"]);
                Console.WriteLine("Longitude : " + jObject["results"][0]["geometry"]["lng"]);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Console.WriteLine("Entered place is not a valid place name!!");
            }
            catch (Exception e)
            {
                Console.WriteLine("Unexpected error occurred!");
            }
        }
    }
}
