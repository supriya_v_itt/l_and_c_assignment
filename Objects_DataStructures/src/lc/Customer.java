package lc;

public class Customer 
{
	private String firstName; 
	private String lastName;
 	private Wallet wallet = new Wallet();
	public String getFirstName()
	{ 
   		return firstName; 
	} 
	public String getLastName()
	{ 
		return lastName; 
	} 
	public float getPayment(float bill)
	{
		float paidMoney = 0.0f;
		if(wallet.getTotalMoney() > bill)
		{
		    wallet.subtractMoney(bill);
			paidMoney = bill;
		}
		return paidMoney;
	}
}
