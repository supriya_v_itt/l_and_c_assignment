package lc;

public class Paperboy 
{
	public static void main(String[] args) 
	{
		float bill = 3.00f;
		Customer customer = new Customer();
		float paidAmount = customer.getPayment(bill);
		if(paidAmount == bill)
		{
			System.out.println("Thank you");
		}
		else
		{
			System.out.println("No sufficient funds");
		}
	}
}
