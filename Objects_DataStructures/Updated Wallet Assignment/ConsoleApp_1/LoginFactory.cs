﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class LoginFactory
    {
        public ILoginChoices GetLoginChoice(string choice)
        {
            if(choice == "1")
            {
                return new UsernamePassword();
            }

            else
            {
                return new Otp();
            }
        }
    }
}
