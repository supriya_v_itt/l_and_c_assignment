﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class Paperboy
    {
		static void Main(String[] args)
		{
			err: float bill = 1.00f;
			Customer customer = new Customer();
			float paidAmount = customer.getPayment(bill);
			if(paidAmount == -1)
            {
				Console.WriteLine("Some error occurred!! Please try again");
				goto err;
            }
			else if (paidAmount == bill)
			{
				Console.WriteLine("Thank you");
			}
			else
			{
				Console.WriteLine("No sufficient funds");
			}
		}
	}
}
