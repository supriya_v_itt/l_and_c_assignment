﻿using System;
using System.Runtime.Serialization.Json;

namespace ConsoleApp1
{
    class Customer
    {
		private String firstName;
		private String lastName;
		private Wallet wallet = new Wallet();
		public String getFirstName()
		{
			return firstName;
		}
		public String getLastName()
		{
			return lastName;
		}
		public float getPayment(float bill)
		{
			LoginFactory loginFactory = new LoginFactory();
			float paidMoney = 0.0f;
			bool success;

			Console.WriteLine("Enter your mode of Login :");
			Console.WriteLine("1.Username and Password \n2.OTP");
			string choice = Console.ReadLine();

			ILoginChoices loginChoices = loginFactory.GetLoginChoice(choice);
			if(choice == "1")
            {
				success = UsernamePassword.success;
            }
			else
            {
				success = Otp.success;
            }

            if (success)
            {
                if (wallet.getTotalMoney() > bill)
                {
                    wallet.subtractMoney(bill);
                    paidMoney = bill;
                }
                return paidMoney;
            }
            else return -1;
        }
	}
}
