Class Employee 
{ 
	string name; 
	int age; 
	float salary; 
	public string getName();
 	void setName(string name); 
	int getAge();
 	void setAge(int age); 
	float getSalary(); 
	void setSalary(float salary);
 }; 

Employee employee;
1. Is 'employee' an object or a data structure? Why? : It is an object, since it has both properties and behaviour.

