﻿using System;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BlogDetails
{
    class DisplayBlogDetails
    {
        static int minimumRange, maximumRange;
        public static void Main(string[] args)
        {
            DisplayBlogDetails blogDetails = new DisplayBlogDetails();
            WebClient webClient = new WebClient();
            Console.WriteLine("Enter the Tumblr blog name:");
            string blogName = Console.ReadLine();
            blogDetails.ValidatingRange();
            string url = "https://" + blogName + ".tumblr.com/api/read/json?type=photo&num=" + maximumRange + "&start=" + minimumRange;
            blogDetails.PrintingData(webClient.DownloadString(url));
        }

        public void ValidatingRange()
        {
            Console.WriteLine("Enter the minimum range:");
            minimumRange = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter the maximum range");
            maximumRange = int.Parse(Console.ReadLine());
            if (maximumRange - minimumRange > 50)
            {
                Console.WriteLine("Maximum range of photos is 50, please try again!!");
                ValidatingRange();
            }
        }

        public void PrintingData(string response)
        {
            while (response.StartsWith("var tumblr_api_read = "))
            {
                response = response.Substring("var tumblr_api_read = ".Length);    //Removing the unwanted characters to make it a valid json
            }
            response = response.Replace(";", "");
            dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(response);
            try
            {
                Console.WriteLine("title :" + dynamicObject["tumblelog"]["title"].ToString());
                Console.WriteLine("name :" + dynamicObject["tumblelog"]["name"].ToString());
                Console.WriteLine("description :" + dynamicObject["tumblelog"]["description"].ToString());
                Console.WriteLine("number of posts :" + dynamicObject["posts-total"].ToString());
                
                foreach (dynamic i in dynamicObject.posts)
                {
                    foreach (dynamic j in i)
                    {
                        if (("" + j).StartsWith("\"photo-url"))
                        {
                            foreach (dynamic k in j)
                            {
                                Console.WriteLine(k);
                            }break;
                        }
                    }
                }
                Console.ReadLine();
            }

            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        
    }
}

