﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Net.Http;
using System.Net.NetworkInformation;
using System.Security.Authentication;
using System.Runtime.ExceptionServices;
using System.Net;

namespace ATM_Exception
{
    class Program
    {
        private static int cardNumber, pin, count = 0, balance = 1000;
        static List<KeyValuePair<int, int>> cardList = new List<KeyValuePair<int, int>>();
        static List<int> blockedCards = new List<int>();
        static void Main(string[] args)
        {
            blockedCards.Add(1111);
            blockedCards.Add(2222);
            cardList.Add(new KeyValuePair<int, int>(1234, 1234));
            cardList.Add(new KeyValuePair<int, int>(5678, 5678));
            Console.WriteLine("Wecome to ATM");
            if (ServerCheck())
            { Initial(); }
        }

        public static bool ServerCheck()
        {
            try
            {
                WebClient client = new WebClient();
                string content = (string)client.DownloadString("http://localhost:3000/");
                return true;
            }
            catch(Exception ex)
            {
                Console.WriteLine("Unable to connect to server, Please try again later");
                return false;
                Logout();
            }
        }

        public static void Initial()
        {
            Console.WriteLine("Please enter your choice");
            Console.WriteLine("1.Login\n2.Generate Pin\n3.Exit");
            switch (Console.ReadLine())
            {
                case ("1"): Login(); break;
                case ("2"): GeneratePin(); break;
                case ("3"): Logout(); break;
                default: break;
            }
        }

        public static bool IsCardBlocked()
        {
            for (int i = 0; i < blockedCards.Count; i++)
            {
                if ((int)blockedCards[i] == cardNumber)
                {
                    return false;
                }
            }
            return true;
        }

        public static void Login()
        {
            try
            {
                if (IsCardBlocked())
                {
                    foreach (KeyValuePair<int, int> data in cardList)
                    {
                        try
                        {
                            count++;
                            Console.WriteLine("Please enter your card number");
                            cardNumber = int.Parse(Console.ReadLine());
                            Console.WriteLine("Please enter your pin");
                            pin = int.Parse(Console.ReadLine());
                            if (data.Key == cardNumber && data.Value == pin)
                            {
                                Options();
                                break;
                            }
                            if (count == cardList.Count)
                            {
                                throw new InvalidCredentialException();
                            }
                        }
                        catch (InvalidCredentialException ex)
                        {
                            blockedCards.Add(cardNumber);
                            Console.WriteLine("Sorry you tried 3 times, Your card is bloced!!");
                            Console.WriteLine("Please contact your bank");
                            Initial();
                        }
                    }
                }
                else
                {
                    throw new BlockedCardException("Sorry your card is blocked!");
                }
            }
            catch (BlockedCardException ex)
            {
                Logout();
            }

        }

        public static void GeneratePin()
        {
            Console.WriteLine("Enter the card number");
            int number = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter the pin of your choice");
            int newPin = int.Parse(Console.ReadLine());
            cardList.Add(new KeyValuePair<int, int>(number, newPin));
            Initial();
        }
        public static void Options()
        {
            Console.WriteLine("Please select your choice");
            Console.WriteLine("--------------------------------------------");
            Console.WriteLine("1.Withdraw Cash\n2.Deposit Cash\n3.Generate Pin\n4.Logout");
            switch (Console.ReadLine())
            {
                case ("1"): WithdrawCash(); break;
                case ("2"): DepositCash(); break;
                case ("3"): GeneratePin(); break;
                case ("4"): Logout(); break;
                default: Logout(); break;
            }
        }

        public static void WithdrawCash()
        {
            Console.WriteLine("Enter the amount to withdraw");
            int amount = int.Parse(Console.ReadLine());
            try
            {
                if (amount <= balance)
                {
                    balance -= amount;
                    Console.WriteLine("You have withdrawn " + amount + " Rs");
                    Console.WriteLine("Your account balance is " + balance);
                    Options();
                }
                else throw new Exception();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Insufficient balance!! Current balance in your account is " + balance);
                Options();
            }
        }

        public static void DepositCash()
        {
            Console.WriteLine("Enter the amount to be deposited");
            int amount = int.Parse(Console.ReadLine());
            balance += amount;
            Console.WriteLine("Your account balance is " + balance);
            Options();
        }

        public static void Logout()
        {
            cardNumber = 0;
            pin = 0;
            Console.WriteLine("Thank you for using our ATM service!! Visit again");
        }
    }

    public class BlockedCardException : Exception
    {
        public BlockedCardException(string message)
        : base(message)
        {
            Console.WriteLine(message);
        }
    }
}
